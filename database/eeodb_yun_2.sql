/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : eeodb_yun

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2014-09-09 23:34:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `eeo_admin`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_admin`;
CREATE TABLE `eeo_admin` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `groupid` tinyint(4) NOT NULL default '1',
  `username` char(10) NOT NULL,
  `realname` char(15) NOT NULL,
  `password` char(32) NOT NULL,
  `lastlogin_time` int(10) unsigned NOT NULL,
  `lastlogin_ip` char(15) NOT NULL,
  `iflock` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usename` (`username`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='管理员信息表';

-- ----------------------------
-- Records of eeo_admin
-- ----------------------------
INSERT INTO `eeo_admin` VALUES ('1', '1', 'admin', 'yunstudio', '168a73655bfecefdb15b14984dd2ad60', '1410265963', '127.0.0.1', '0');
INSERT INTO `eeo_admin` VALUES ('8', '6', 'user1', 'yz', '165dad32183729915cfa208d6e1b4fed', '1410265937', '127.0.0.1', '0');

-- ----------------------------
-- Table structure for `eeo_answer`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_answer`;
CREATE TABLE `eeo_answer` (
  `id` int(11) NOT NULL auto_increment COMMENT '答案id',
  `member_id` int(11) NOT NULL COMMENT '考生id',
  `quiz_id` int(11) NOT NULL COMMENT '测试id',
  `content` text NOT NULL COMMENT '答案内容',
  `score` int(11) NOT NULL default '0' COMMENT '本题总分',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='答案表';

-- ----------------------------
-- Records of eeo_answer
-- ----------------------------

-- ----------------------------
-- Table structure for `eeo_fragment`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_fragment`;
CREATE TABLE `eeo_fragment` (
  `id` int(10) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `sign` varchar(255) NOT NULL COMMENT '前台调用标记',
  `content` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_fragment
-- ----------------------------
INSERT INTO `eeo_fragment` VALUES ('1', '右侧公告信息', 'announce', '<p>\r\n	本站为Yuncms的默认演示模板，Yuncms是一款基于PHP+MYSQL构建的高效网站管理系统。 后台地址请在网址后面加上/index.php?yun=admin进入。 后台的用户名:admin;密码:123456，请进入后修改默认密码。\r\n</p>\r\n<p>\r\n	<img src=\"/yuncms/upload/fragment/image/20140224/20140224192956_37828.jpg\" width=\"100\" height=\"120\" alt=\"\" /> \r\n</p>');

-- ----------------------------
-- Table structure for `eeo_group`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_group`;
CREATE TABLE `eeo_group` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `power` varchar(1000) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_group
-- ----------------------------
INSERT INTO `eeo_group` VALUES ('1', '超级管理员', '-1');
INSERT INTO `eeo_group` VALUES ('2', '普通管理员', '277,283,1,2,4,5,6,7,8,9,228,10,11,12,13,14,15,16,157,158,174,268,288');
INSERT INTO `eeo_group` VALUES ('3', 'test', '277,283,1,2,4,5,6,7,8,9,228');
INSERT INTO `eeo_group` VALUES ('4', 'admin1', '23,24,25,26,27');
INSERT INTO `eeo_group` VALUES ('6', '学生', '283,22,23,24,25,26,27');

-- ----------------------------
-- Table structure for `eeo_link`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_link`;
CREATE TABLE `eeo_link` (
  `id` int(10) NOT NULL auto_increment,
  `type` tinyint(1) NOT NULL COMMENT '类型',
  `norder` int(5) NOT NULL COMMENT '排序',
  `name` varchar(30) NOT NULL COMMENT '站点名',
  `url` varchar(40) NOT NULL COMMENT '站点地址',
  `picture` varchar(30) NOT NULL COMMENT '本地logo',
  `logourl` varchar(50) NOT NULL COMMENT '远程logo',
  `siteowner` varchar(30) NOT NULL COMMENT '站点所有者',
  `info` varchar(300) NOT NULL COMMENT '介绍',
  `ispass` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_link
-- ----------------------------
INSERT INTO `eeo_link` VALUES ('2', '2', '0', '云作坊', 'http://www.yunstudio.net', '1342232581.png', '', '云作坊', '', '1');
INSERT INTO `eeo_link` VALUES ('6', '1', '0', '科技交流平台', 'http://www.kjjlpt.com', '', '', '', '', '1');

-- ----------------------------
-- Table structure for `eeo_members`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_members`;
CREATE TABLE `eeo_members` (
  `id` int(20) NOT NULL auto_increment,
  `groupid` int(3) NOT NULL,
  `account` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `qq` varchar(20) NOT NULL,
  `schoolid` int(11) default NULL COMMENT '学校id',
  `snum` varchar(50) default NULL COMMENT '学号或教师编号',
  `idcard` varchar(100) default NULL COMMENT '身份证号码',
  `gender` int(11) NOT NULL default '0' COMMENT '性别（0：未知，1：男，2：女）',
  `regtime` int(11) NOT NULL,
  `regip` varchar(16) NOT NULL,
  `lasttime` int(11) NOT NULL,
  `lastip` varchar(16) NOT NULL,
  `islock` tinyint(4) NOT NULL COMMENT '冻结(锁定):1 开启(解锁):0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_members
-- ----------------------------
INSERT INTO `eeo_members` VALUES ('5', '2', 'user1', 'e78271e58d6e5eb71a5acf378f040322', '杨真', 'yzknight@126.com', '', '', null, null, null, '0', '1410185947', '127.0.0.1', '1410269165', '127.0.0.1', '0');

-- ----------------------------
-- Table structure for `eeo_member_group`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_member_group`;
CREATE TABLE `eeo_member_group` (
  `id` int(3) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `notallow` text NOT NULL,
  `bindsort` text COMMENT '绑定的栏目id列表，以|分隔,如果不为空，则可以在相应的栏目上传文档',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_member_group
-- ----------------------------
INSERT INTO `eeo_member_group` VALUES ('2', '学生', 'class', null);
INSERT INTO `eeo_member_group` VALUES ('4', '老师', '', null);

-- ----------------------------
-- Table structure for `eeo_method`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_method`;
CREATE TABLE `eeo_method` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `rootid` int(10) unsigned NOT NULL,
  `pid` float unsigned NOT NULL,
  `operate` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ifmenu` tinyint(1) NOT NULL default '0' COMMENT '是否菜单显示',
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=320 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_method
-- ----------------------------
INSERT INTO `eeo_method` VALUES ('1', '1', '0', 'admin', '后台登陆管理', '1');
INSERT INTO `eeo_method` VALUES ('2', '1', '1', 'index', '管理员管理', '1');
INSERT INTO `eeo_method` VALUES ('4', '1', '1', 'admindel', '管理员删除', '0');
INSERT INTO `eeo_method` VALUES ('5', '1', '1', 'adminedit', '管理员编辑', '0');
INSERT INTO `eeo_method` VALUES ('6', '1', '1', 'adminlock', '管理员锁定', '0');
INSERT INTO `eeo_method` VALUES ('7', '1', '1', 'group', '权限管理', '1');
INSERT INTO `eeo_method` VALUES ('8', '1', '1', 'groupedit', '管理组编辑', '0');
INSERT INTO `eeo_method` VALUES ('9', '1', '1', 'groupdel', '管理组删除', '0');
INSERT INTO `eeo_method` VALUES ('10', '10', '0', 'news', '资讯管理', '1');
INSERT INTO `eeo_method` VALUES ('11', '10', '10', 'index', '已有资讯', '1');
INSERT INTO `eeo_method` VALUES ('12', '10', '10', 'add', '添加资讯', '1');
INSERT INTO `eeo_method` VALUES ('13', '10', '10', 'edit', '资讯编辑', '0');
INSERT INTO `eeo_method` VALUES ('14', '10', '10', 'del', '资讯删除', '0');
INSERT INTO `eeo_method` VALUES ('15', '10', '10', 'lock', '资讯锁定', '0');
INSERT INTO `eeo_method` VALUES ('16', '10', '10', 'recmd', '资讯推荐', '0');
INSERT INTO `eeo_method` VALUES ('17', '17', '0', 'dbback', '数据库管理', '1');
INSERT INTO `eeo_method` VALUES ('18', '17', '17', 'index', '数据库备份', '1');
INSERT INTO `eeo_method` VALUES ('19', '17', '17', 'recover', '备份恢复', '0');
INSERT INTO `eeo_method` VALUES ('20', '17', '17', 'detail', '备份详细', '0');
INSERT INTO `eeo_method` VALUES ('21', '17', '17', 'del', '备份删除', '0');
INSERT INTO `eeo_method` VALUES ('22', '22', '0', 'index', '后台面板', '1');
INSERT INTO `eeo_method` VALUES ('23', '22', '22', 'index', '后台首页', '0');
INSERT INTO `eeo_method` VALUES ('24', '22', '22', 'login', '登陆', '0');
INSERT INTO `eeo_method` VALUES ('25', '22', '22', 'logout', '退出登陆', '0');
INSERT INTO `eeo_method` VALUES ('26', '22', '22', 'verify', '验证码', '0');
INSERT INTO `eeo_method` VALUES ('27', '22', '22', 'welcome', '服务器环境', '0');
INSERT INTO `eeo_method` VALUES ('28', '28', '0', 'set', '全局设置', '1');
INSERT INTO `eeo_method` VALUES ('29', '28', '28', 'index', '网站设置', '1');
INSERT INTO `eeo_method` VALUES ('30', '30', '0', 'sort', '分类管理', '1');
INSERT INTO `eeo_method` VALUES ('31', '30', '30', 'index', '栏目列表', '1');
INSERT INTO `eeo_method` VALUES ('33', '30', '30', 'del', '分类删除', '0');
INSERT INTO `eeo_method` VALUES ('317', '317', '0', 'msg', '答疑管理', '1');
INSERT INTO `eeo_method` VALUES ('85', '28', '28', 'menuname', '后台功能', '1');
INSERT INTO `eeo_method` VALUES ('159', '150', '150', 'images_upload', '图片批量上传', '0');
INSERT INTO `eeo_method` VALUES ('158', '10', '10', 'FileManagerJson', '编辑器上传管理', '0');
INSERT INTO `eeo_method` VALUES ('157', '10', '10', 'UploadJson', '编辑器上传', '0');
INSERT INTO `eeo_method` VALUES ('174', '10', '10', 'cutcover', '封面图剪切', '0');
INSERT INTO `eeo_method` VALUES ('236', '30', '30', 'PageUploadJson', '单页上传', '0');
INSERT INTO `eeo_method` VALUES ('235', '30', '30', 'pageedit', '单页编辑', '0');
INSERT INTO `eeo_method` VALUES ('234', '30', '30', 'pageadd', '添加单页栏目', '0');
INSERT INTO `eeo_method` VALUES ('231', '30', '30', 'newsedit', '文章栏目编辑', '0');
INSERT INTO `eeo_method` VALUES ('230', '30', '30', 'newsadd', '添加文章栏目', '0');
INSERT INTO `eeo_method` VALUES ('182', '28', '28', 'clear', '网站缓存', '1');
INSERT INTO `eeo_method` VALUES ('188', '188', '0', 'link', '友情链接', '1');
INSERT INTO `eeo_method` VALUES ('189', '188', '188', 'index', '链接列表', '1');
INSERT INTO `eeo_method` VALUES ('190', '188', '188', 'add', '添加链接', '1');
INSERT INTO `eeo_method` VALUES ('191', '188', '188', 'edit', '链接编辑', '0');
INSERT INTO `eeo_method` VALUES ('192', '188', '188', 'del', '链接删除', '0');
INSERT INTO `eeo_method` VALUES ('228', '1', '1', 'adminnow', '账户管理', '1');
INSERT INTO `eeo_method` VALUES ('229', '188', '188', 'lock', '锁定', '0');
INSERT INTO `eeo_method` VALUES ('237', '30', '30', 'PageFileManagerJson', '单页上传管理', '0');
INSERT INTO `eeo_method` VALUES ('238', '238', '0', 'fragment', '碎片管理', '1');
INSERT INTO `eeo_method` VALUES ('239', '238', '238', 'index', '碎片列表', '1');
INSERT INTO `eeo_method` VALUES ('240', '238', '238', 'add', '碎片添加', '1');
INSERT INTO `eeo_method` VALUES ('241', '238', '238', 'edit', '碎片编辑', '0');
INSERT INTO `eeo_method` VALUES ('242', '238', '238', 'del', '碎片删除', '0');
INSERT INTO `eeo_method` VALUES ('243', '238', '238', 'UploadJson', '编辑器上传', '0');
INSERT INTO `eeo_method` VALUES ('244', '238', '238', 'FileManagerJson', '编辑器上传管理', '0');
INSERT INTO `eeo_method` VALUES ('245', '28', '28', 'tpchange', '前台模板', '1');
INSERT INTO `eeo_method` VALUES ('251', '30', '30', 'pluginadd', '添加应用栏目', '0');
INSERT INTO `eeo_method` VALUES ('252', '30', '30', 'pluginedit', '应用栏目编辑', '0');
INSERT INTO `eeo_method` VALUES ('267', '258', '258', 'file', '文件上传', '0');
INSERT INTO `eeo_method` VALUES ('288', '10', '10', 'colchange', '资讯转移栏目', '0');
INSERT INTO `eeo_method` VALUES ('283', '0', '0', 'member', '用户管理', '1');
INSERT INTO `eeo_method` VALUES ('292', '28', '28', 'tplist', '模板文件列表', '0');
INSERT INTO `eeo_method` VALUES ('293', '28', '28', 'tpadd', '模板文件添加', '0');
INSERT INTO `eeo_method` VALUES ('294', '28', '28', 'tpedit', '模板文件编辑', '0');
INSERT INTO `eeo_method` VALUES ('295', '28', '28', 'tpdel', '删除模板文件', '0');
INSERT INTO `eeo_method` VALUES ('296', '28', '28', 'tpgetcode', '获取模板内容', '0');
INSERT INTO `eeo_method` VALUES ('301', '30', '30', 'add', '添加栏目', '1');
INSERT INTO `eeo_method` VALUES ('304', '30', '30', 'placelist', '内容定位列表', '1');
INSERT INTO `eeo_method` VALUES ('305', '30', '30', 'placeadd', '添加内容定位', '1');
INSERT INTO `eeo_method` VALUES ('306', '30', '30', 'placeedit', '定位编辑', '0');
INSERT INTO `eeo_method` VALUES ('307', '30', '30', 'placedel', '定位删除', '0');
INSERT INTO `eeo_method` VALUES ('308', '308', '0', 'tags', 'TAG标签', '1');
INSERT INTO `eeo_method` VALUES ('309', '308', '308', 'index', '标签列表', '1');
INSERT INTO `eeo_method` VALUES ('310', '308', '308', 'del', '删除标签', '0');
INSERT INTO `eeo_method` VALUES ('311', '308', '308', 'hits', '编辑点击量', '0');
INSERT INTO `eeo_method` VALUES ('312', '308', '308', 'add', '生成标签', '1');
INSERT INTO `eeo_method` VALUES ('313', '308', '308', 'mesup', '文档数量更新', '0');
INSERT INTO `eeo_method` VALUES ('314', '314', '0', 'files', '附件管理', '1');
INSERT INTO `eeo_method` VALUES ('315', '314', '314', 'index', '文件列表', '1');
INSERT INTO `eeo_method` VALUES ('316', '314', '314', 'del', '删除文件', '0');
INSERT INTO `eeo_method` VALUES ('318', '317', '317', 'index', '答疑列表', '1');
INSERT INTO `eeo_method` VALUES ('319', '317', '317', 'del', '删除答疑', '0');

-- ----------------------------
-- Table structure for `eeo_msg`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_msg`;
CREATE TABLE `eeo_msg` (
  `id` int(11) NOT NULL auto_increment COMMENT '答疑id',
  `title` varchar(255) default NULL COMMENT '答疑标题',
  `content` text NOT NULL COMMENT '答疑内容',
  `account` varchar(50) default NULL COMMENT '管理员或者注册用户帐号',
  `name` varchar(50) default NULL COMMENT '用户名称',
  `state` int(11) NOT NULL default '1' COMMENT '状态 1:正常2:锁定',
  `rootid` int(11) NOT NULL default '0' COMMENT '提问id',
  `parentid` int(11) NOT NULL default '0' COMMENT '父级id,没有则为提问,有则为回复',
  `addtime` datetime NOT NULL COMMENT '添加时间',
  `islock` tinyint(4) NOT NULL default '0' COMMENT '是否锁定0:否,1:是',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='答疑表';

-- ----------------------------
-- Records of eeo_msg
-- ----------------------------
INSERT INTO `eeo_msg` VALUES ('11', null, '哈哈啊', 'user1', '杨真', '1', '0', '10', '2014-09-09 22:42:45', '0');
INSERT INTO `eeo_msg` VALUES ('10', null, '请问?', 'user1', '杨真', '1', '0', '0', '2014-09-09 22:42:38', '0');
INSERT INTO `eeo_msg` VALUES ('12', null, 'hahahha', 'admin(管理员)', 'yunstudio', '1', '0', '10', '0000-00-00 00:00:00', '0');
INSERT INTO `eeo_msg` VALUES ('14', null, 'sacsa', 'admin(管理员)', 'yunstudio', '1', '0', '10', '2014-09-09 23:32:07', '0');
INSERT INTO `eeo_msg` VALUES ('15', null, 'ada', 'admin(管理员)', 'yunstudio', '1', '0', '10', '0000-00-00 00:00:00', '0');

-- ----------------------------
-- Table structure for `eeo_news`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_news`;
CREATE TABLE `eeo_news` (
  `id` int(20) NOT NULL auto_increment,
  `sort` varchar(350) NOT NULL COMMENT '类别',
  `account` char(15) NOT NULL COMMENT '发布者账户',
  `title` varchar(60) NOT NULL COMMENT '标题',
  `places` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL COMMENT '标题颜色',
  `picture` varchar(80) NOT NULL,
  `keywords` varchar(300) NOT NULL COMMENT '关键字',
  `description` varchar(600) NOT NULL,
  `content` text NOT NULL COMMENT '内容',
  `method` varchar(100) NOT NULL COMMENT '方法',
  `tpcontent` varchar(100) NOT NULL COMMENT '模板',
  `norder` int(4) NOT NULL COMMENT '排序',
  `recmd` tinyint(1) NOT NULL COMMENT '推荐',
  `hits` int(10) NOT NULL COMMENT '点击量',
  `ispass` tinyint(1) NOT NULL,
  `origin` varchar(30) NOT NULL COMMENT '来源',
  `addtime` int(11) NOT NULL,
  `extfield` int(10) NOT NULL default '0' COMMENT '拓展字段',
  PRIMARY KEY  (`id`),
  FULLTEXT KEY `sort` (`sort`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_news
-- ----------------------------
INSERT INTO `eeo_news` VALUES ('29', ',000000,100001', 'admin', '百度音乐桌面版', '100,101', '', 'NoPic.gif', '', '', '<embed src=\"/git/electricEduOnline/trunk_yun/upload/news/media/20140909/20140909211558_80230.mp4\" type=\"video/x-ms-asf-plugin\" width=\"550\" height=\"400\" autostart=\"false\" loop=\"true\" />', 'news/content', 'news_content', '0', '0', '35', '1', '原创', '1410268543', '0');

-- ----------------------------
-- Table structure for `eeo_page`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_page`;
CREATE TABLE `eeo_page` (
  `id` int(10) NOT NULL auto_increment,
  `sort` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `edittime` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_page
-- ----------------------------
INSERT INTO `eeo_page` VALUES ('1', ',000000,100006', '联系方式&nbsp;', '2014-09-08 16:07:48');

-- ----------------------------
-- Table structure for `eeo_place`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_place`;
CREATE TABLE `eeo_place` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL,
  `norder` int(5) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_place
-- ----------------------------
INSERT INTO `eeo_place` VALUES ('100', '首页banner', '0');
INSERT INTO `eeo_place` VALUES ('101', '首页幻灯', '0');

-- ----------------------------
-- Table structure for `eeo_school`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_school`;
CREATE TABLE `eeo_school` (
  `id` int(11) NOT NULL auto_increment COMMENT '学校id',
  `name` varchar(255) NOT NULL COMMENT '学校名称',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学校表';

-- ----------------------------
-- Records of eeo_school
-- ----------------------------

-- ----------------------------
-- Table structure for `eeo_sort`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_sort`;
CREATE TABLE `eeo_sort` (
  `id` int(6) unsigned NOT NULL auto_increment,
  `type` tinyint(2) unsigned NOT NULL default '0' COMMENT '模型类别',
  `path` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `deep` int(5) unsigned NOT NULL default '1' COMMENT '深度',
  `norder` tinyint(10) unsigned NOT NULL default '0' COMMENT '排序',
  `ifmenu` tinyint(1) NOT NULL COMMENT '是否前台显示',
  `method` varchar(100) NOT NULL COMMENT '模型方法',
  `tplist` varchar(100) NOT NULL COMMENT '列表模板',
  `keywords` varchar(255) NOT NULL COMMENT '描述',
  `description` varchar(300) NOT NULL COMMENT '描述',
  `url` varchar(100) NOT NULL COMMENT '外部链接',
  `extendid` int(10) default NULL COMMENT '拓展表id',
  PRIMARY KEY  (`id`),
  FULLTEXT KEY `path` (`path`)
) ENGINE=MyISAM AUTO_INCREMENT=100011 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_sort
-- ----------------------------
INSERT INTO `eeo_sort` VALUES ('100001', '1', ',000000', '基地概况', '1', '0', '1', 'news/index', 'news_index,news_content', '网络教学,长沙理工大学,电气与信息工程学院', '网络教学,长沙理工大学,电气与信息工程学院', '10', '0');
INSERT INTO `eeo_sort` VALUES ('100002', '1', ',000000', '实践教学', '1', '3', '1', 'news/index', 'news_index,news_content', '网络教学,长沙理工大学,电气与信息工程学院', '网络教学,长沙理工大学,电气与信息工程学院', '10', '0');
INSERT INTO `eeo_sort` VALUES ('100003', '1', ',000000', '管理制度', '1', '1', '1', 'news/index', 'news_index,news_content', '网络教学,长沙理工大学,电气与信息工程学院', '网络教学,长沙理工大学,电气与信息工程学院', '10', '0');
INSERT INTO `eeo_sort` VALUES ('100004', '1', ',000000', '网络教室', '1', '2', '1', 'news/index', 'news_index,news_content', '网络教学,长沙理工大学,电气与信息工程学院', '网络教学,长沙理工大学,电气与信息工程学院', '10', '0');
INSERT INTO `eeo_sort` VALUES ('100010', '5', ',000000', '在线答疑', '1', '4', '1', '', '', '', '', 'default/msg/index', '0');
INSERT INTO `eeo_sort` VALUES ('100006', '3', ',000000', '联系方式 ', '1', '5', '1', 'page/index', 'page_index', '网络教学,长沙理工大学,电气与信息工程学院', '网络教学,长沙理工大学,电气与信息工程学院', '', null);

-- ----------------------------
-- Table structure for `eeo_tags`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_tags`;
CREATE TABLE `eeo_tags` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL,
  `hits` int(10) NOT NULL default '0',
  `mesnum` int(10) NOT NULL default '0',
  `addtime` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eeo_tags
-- ----------------------------
