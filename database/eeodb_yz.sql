/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : eeodb

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2014-09-06 21:33:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `eeo_user`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_user`;
CREATE TABLE `eeo_user` (
  `id` int NOT NULL auto_increment COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '登录名',
  `password` varchar(50) NOT NULL COMMENT '用户密码',
  `email` varchar(100) NOT NULL COMMENT '用户邮箱（用于找回用户名和重置密码）',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `school_id` int NOT NULL COMMENT '学校id',
  `major` varchar(255) COMMENT '专业',
   `s_number` varchar(100) NOT NULL COMMENT '身份证号码',
   `id_card` varchar(100) NOT NULL COMMENT '身份证号码',
  `role_id` int NOT NULL COMMENT '用户角色id',
  `state` int(11) NOT NULL default 1 COMMENT '状态（1：正常，2:审核3：锁定）',
  `add_time` datetime NOT NULL COMMENT '注册时间',
  `gender` int(11) NOT NULL default 0 COMMENT '性别（0：未知，1：男，2：女）',
  `grade` varchar(10) COMMENT '年级（例:2012代表2012级）',
  `qq` varchar(50) default NULL COMMENT 'QQ号码',
  `phone` varchar(50) default NULL COMMENT '手机号码',
  `last_login_ip` varchar(30) default NULL COMMENT '上次登录ip',
  `last_login_time` datetime default NULL COMMENT '上次登录时间',
  PRIMARY KEY  (`id`),
  UNIQUE KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='前台用户表';

-- ----------------------------
-- Table structure for `eeo_admin`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_admin`;
CREATE TABLE `eeo_admin` (
  `id` int NOT NULL auto_increment COMMENT '管理员id',
  `username` varchar(50) NOT NULL COMMENT '登录名',
  `password` varchar(50) NOT NULL COMMENT '用户密码',
  `email` varchar(100) NOT NULL COMMENT '用户邮箱（用于找回用户名和重置密码）',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `role_id` int NOT NULL COMMENT '管理员角色id',
  `state` int(11) NOT NULL default 1 COMMENT '状态(0：正常，1：锁定)',
  `add_time` datetime NOT NULL COMMENT '注册时间',
  `gender` int(11) NOT NULL default 0 COMMENT '性别',
  `school_id` int NOT NULL COMMENT '学校id',
  `major` varchar(255) default NULL COMMENT '专业',
  `qq` varchar(50) default NULL COMMENT 'QQ号码',
  `phone` varchar(50) default NULL COMMENT '手机号码',
  `last_login_ip` varchar(30) default NULL COMMENT '上次登录ip',
  `last_login_time` datetime default NULL COMMENT '上次登录时间',
  PRIMARY KEY  (`id`),
  UNIQUE KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='后台管理员表';


-- ----------------------------
-- Table structure for `eeo_school`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_school`;
CREATE TABLE `eeo_school` (
  `id` int NOT NULL auto_increment COMMENT '学校id',
  `name` varchar(255) NOT NULL COMMENT '学校名称',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学校表';


-- ----------------------------
-- Table structure for `eeo_column`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_column`;
CREATE TABLE `eeo_column` (
  `id` int NOT NULL auto_increment COMMENT '栏目id',
  `title` varchar(255) NOT NULL COMMENT '栏目名称',
  `description` varchar(255) default NULL COMMENT '栏目描述',
  `admin_id` int NOT NULL COMMENT '操作的管理员id',
  `default_article_id` int NOT NULL COMMENT '栏目介绍文章的id',
  `parent_id` int DEFAULT 0 COMMENT '栏目介绍文章的id',
  `sort` int NOT NULL DEFAULT 0 COMMENT '排序权值，权值越大越靠前',
  `position` int NOT NULL default 1 COMMENT '位置(1.导航栏目 2.首页栏目)',
  `type` int NOT NULL default 1 COMMENT '类型(1.文档模型2.单页模型3.在线答疑),当有子分类时此字段无效，指定为单页模型',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `tpl_column_id` int NOT NULL COMMENT '二级模版id',
  `tpl_ariticle_id` int NOT NULL COMMENT '三级模版id',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='栏目表';


-- ----------------------------
-- Table structure for `eeo_template`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_template`;
CREATE TABLE `eeo_template` (
  `id` int NOT NULL auto_increment COMMENT '模版id',
  `name` varchar(255) NOT NULL COMMENT '模版名称',
  `description` varchar(255) COMMENT '模版描述',
  `admin_id` int NOT NULL COMMENT '操作的管理员id',
  `path` varchar(1024) NOT NULL COMMENT '模版路径(view之后的部分)',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='模版表';



-- ----------------------------
-- Table structure for `eeo_article`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_article`;
CREATE TABLE `eeo_article` (
  `id` int NOT NULL auto_increment COMMENT '文章id',
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `description` varchar(255) COMMENT '文章摘要',
  `content` text NOT NULL COMMENT '文章内容',
  `column_id` int NOT NULL COMMENT '栏目id(删除栏目时可置0)',
  `author` varchar(50) NOT NULL COMMENT '作者',
  `state` int NOT NULL default 1  COMMENT '状态 1:发布2:审核3:删除',
  `is_slide` bit NOT NULL default 0  COMMENT '是否在首页轮播 0:否 1:是',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `publish_time` datetime NOT NULL COMMENT '发布时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `last_user_id` int NOT NULL COMMENT '上次操作的用户id(老师)',
  `last_admin_id` int NOT NULL COMMENT '上次操作的管理员id',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章表';


-- ----------------------------
-- Table structure for `eeo_qa`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_qa`;
CREATE TABLE `eeo_qa` (
  `id` int NOT NULL auto_increment COMMENT '答疑id',
  `title` varchar(255) NOT NULL COMMENT '答疑标题',
  `content` text NOT NULL COMMENT '答疑内容',
  `user_id` int NOT NULL COMMENT '用户id',
  `state` int NOT NULL default 1  COMMENT '状态 1:正常2:锁定',
  `parent_id` int COMMENT '父级id,没有则为提问,有则为回复',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='答疑表';

-- ----------------------------
-- Table structure for `eeo_quiz`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_quiz`;
CREATE TABLE `eeo_quiz` (
  `id` int NOT NULL auto_increment COMMENT '测试id',
  `title` varchar(255) NOT NULL COMMENT '测试标题',
  `school_id` int COMMENT '学校id,为空则指定学校,不为空则不指定学校' ,
  `user_id` int NOT NULL COMMENT '发起用户id(仅老师)',
  `state` int NOT NULL default 1  COMMENT '状态1. 尚未开始2. 已经开始3.已经结束',
  `start_time` bigint NOT NULL COMMENT '测试时间',
  `duration` bigint NOT NULL COMMENT '测试持续',
  `students` varchar(2048) NOT NULL COMMENT '考生学号列表1. 为空则不限制参加人员，必须同时指定学校和考生才能启用自动统计未参考考生2. 支持简略写法，比如2012xxx-2012xxx,2012xxx,',
  `is_stu_view_answer` bit NOT NULL default 0 COMMENT '考生提交答案后是否可以立即查看答案 0.否1.是 ',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='测试表';



-- ----------------------------
-- Table structure for `eeo_question`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_question`;
CREATE TABLE `eeo_question` (
  `id` int NOT NULL auto_increment COMMENT '问题id',
  `type` int NOT NULL default 1  COMMENT '题型1. 选择题2. 问答题',
  `content` text NOT NULL COMMENT '问题内容',
  `answer` text COMMENT '答案，其中选择题答案多选以，隔开' ,
  `score` int NOT NULL default 0  COMMENT '本题总分',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='试题表';




-- ----------------------------
-- Table structure for `eeo_quiz_question`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_quiz_question`;
CREATE TABLE `eeo_quiz_question` (
  `quiz_id` int NOT NULL COMMENT '测试id',
  `question_id` int NOT NULL COMMENT '试题id',
  PRIMARY KEY  (`quiz_id`,`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='测试试题关系表';



-- ----------------------------
-- Table structure for `eeo_answer`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_answer`;
CREATE TABLE `eeo_answer` (
  `id` int NOT NULL auto_increment COMMENT '答案id',
  `user_id` int NOT NULL COMMENT '考生id',
  `quiz_id` int NOT NULL COMMENT '测试id',
  `content` text NOT NULL COMMENT '答案内容',
  `score` int NOT NULL default 0  COMMENT '本题总分',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='答案表';




-- ----------------------------
-- Table structure for `eeo_params`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_params`;
CREATE TABLE `eeo_params` (
  `id` int NOT NULL auto_increment COMMENT '配置id',
  `t_regist_check` int NOT NULL default 1 COMMENT '老师注册是否需要审核(1：不需要，2：需要)',
  `s_regist_check` int NOT NULL default 1 COMMENT '学生注册是否需要审核(1：不需要，2：需要)',
  `t_publish_check` int NOT NULL default 1 COMMENT '老师发布文章是否需要审核(1：不需要，2：需要 )',
  `qa_check` int NOT NULL default 1 COMMENT '老师发布文章是否需要审核(1：不需要，2：需要 )',
  `max_size_file` int NOT NULL default 20 COMMENT '最大上传文件,单位mb',
  `max_size_video` int NOT NULL default 500 COMMENT '最大上传视频,单位mb',
  `v_download` int NOT NULL default 1 COMMENT '游客是否可以下载(1：不可以，2：可以)',
  `v_video` int NOT NULL default 1 COMMENT '游客是否可以浏览视频(1：不可以，2：可以)',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统配置表';





-- ----------------------------
-- Table structure for `eeo_ip`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_ip`;
CREATE TABLE `eeo_ip` (
  `id` int NOT NULL auto_increment COMMENT '屏蔽ip id',
  `ip` varchar(50) NOT NULL COMMENT 'ip 地址',
  `description` varchar(50) COMMENT '说明',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='屏蔽ip表';



-- ----------------------------
-- Table structure for `eeo_log`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_log`;
CREATE TABLE `eeo_log` (
  `id` int NOT NULL auto_increment COMMENT '操作id',
  `admin_id` int NOT NULL COMMENT '操作管理员id',
  `opt_time` datetime NOT NULL COMMENT '添加时间',
  `opt_ip` varchar(50) NOT NULL COMMENT '操作ip',
  `opt_type` varchar(50) NOT NULL COMMENT '操作类型',
  `description` varchar(255)  COMMENT '操作内容',
  `operator` varchar(50) default 1 COMMENT '操作人姓名',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志表';



-- ----------------------------
-- Table structure for `eeo_role`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_role`;
CREATE TABLE `eeo_role` (
  `id` int NOT NULL auto_increment COMMENT '角色id',
  `name` varchar(100) NOT NULL COMMENT '角色名称',
  `description` varchar(255) COMMENT '角色说明',
  `sort` int default 0 COMMENT '排序权值',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色表';


-- ----------------------------
-- Table structure for `eeo_permission`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_permission`;
CREATE TABLE `eeo_permission` (
  `id` int NOT NULL auto_increment COMMENT '权限id',
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `type` int  COMMENT '权限类型',
  `description` varchar(255) COMMENT '权限说明',
  `sort` int default 0 COMMENT '排序权值',
  `uri` varchar(100) default 0 COMMENT 'uri(从模块开始)',
  `rule` varchar(100) default 0 COMMENT '规则(where子句)',
  PRIMARY KEY  (`id`),
  UNIQUE KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限表';


-- ----------------------------
-- Table structure for `eeo_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `eeo_role_permission`;
CREATE TABLE `eeo_role_permission` (
  `role_id` int NOT NULL COMMENT '角色id',
  `permission_id` int NOT NULL COMMENT '权限id',
  PRIMARY KEY  (`role_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';


-- ----------------------------
-- 插入默认权限数据
-- ----------------------------
-- insert into `eeo_permission`(id,name,type,description,sort,uri,rule) values(1,'添加前台用户',1)








