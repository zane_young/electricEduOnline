<?php
namespace Admin\Model;
use Think\Model;
class SchoolModel extends Model {
	public function updateCache() {
		$schoolList = $this -> select();
		S(SCHOOL_LIST,$schoolList);
		return $schoolList;
	}

	public function findAll() {
		$schoolList = S(SCHOOL_LIST);
		if (!$schoolList||APP_DEBUG) {
			$schoolList=$this->updateCache();
		}
		return $schoolList;
	}

	//填充所有school数据
	public static function assignSchoolList($c) {
		$School = D('School');
		$schoolList = $School -> findAll();
		$c -> __set('schoolList', json_encode($schoolList));
	}

}
