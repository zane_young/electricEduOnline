<?php
namespace Admin\Controller;
use Think\Controller;
class UserController extends Controller {
    public function index(){
    	\Admin\Model\SchoolModel::assignSchoolList($this);
    	$this->display();
    }
	public function data(){
		$User=M('User');
		
		$Page=new \Lib\LigerPage();
		$where=$Page->where;
		$count=$User->where($where)->count();
		$Page->init($count);
		
		$rows=$User->table('__USER__ as user')
			->where($where)
			->join('left join __SCHOOL__ as school on user.school_id=school.id')
			->join('left join __ROLE__ as role on user.role_id=role.id')
			->field('user.id,user.username,user.name as name,school.name as school,user.state')
			->limit($Page->firstRow,$Page->pageSize)
			->select();
		\Lib\Util::tplog($rows);
		if(!$rows){
			$rows=array();
		}
		$data=array('Rows'=>$rows,'Total'=>$count);
		$this->ajaxReturn($data);
	}
}

	

