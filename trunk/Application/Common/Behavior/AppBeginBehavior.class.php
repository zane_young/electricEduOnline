<?php

namespace Common\Behavior;

class AppBeginBehavior{
	protected $options=array(
    'USER_AUTH_ON'        =>false,   //  是否开启用户认证
    'USER_AUTH_ID'        => 'user_id',   //  定义用户的id为权限认证字段
	);
	public function run(){
            if(APP_DEBUG){
                //debug模式下自动创建log路径,修复ThinkPHP删除Rungtime目录之后无法记录日志的bug
                $logpath=C('LOG_PATH');
                if(!file_exists($logpath)){
                    mkdir($logpath);
                }
            }
	}
}
