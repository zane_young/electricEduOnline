<?php
return array(
	//'配置项'=>'配置值'
	//db
	'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  '127.0.0.1', // 服务器地址
    'DB_NAME'               =>  'eeodb',          // 数据库名
    'DB_USER'               =>  'yun',      // 用户名
    'DB_PWD'                =>  'yun',          // 密码
    'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  'eeo_',    // 数据库表前缀
    
    'COOKIE_HTTPONLY'       =>  TRUE,      // Cookie httponly设置
    
//  'SHOW_PAGE_TRACE' => TRUE,
    'TMPL_LOAD_DEFAULTTHEME' => true, //如果在该主题下找不到对应的模板文件，则使用默认的模板文件
    'TMPL_PARSE_STRING'  =>array(     
//  	'__PUBLIC__' => '/Common', // 更改默认的/Public 替换规则     
//  	'__JS__'     => '/Public/JS/', // 增加新的JS类库路径替换规则     
//  	'__UPLOAD__' => '/Uploads', // 增加新的上传路径替换规则
    ),
    
    //url模式为2:rewrite模式，可以隐藏index.php,1不隐藏index.php
    'URL_MODEL' => 1,
    'DEFAULT_MODULE'       =>    'Home',  // 默认模块
    //忽略url的大小写
    'URL_CASE_INSENSITIVE' => true,
    
	//权限
	'ADMINISTRATOR'=>array('1'),
    
	 
    /* 数据缓存设置 */
    'DATA_CACHE_TIME'       =>  600,      // 数据缓存有效期 0表示永久缓存
    'DATA_CACHE_COMPRESS'   =>  false,   // 数据缓存是否压缩缓存
    'DATA_CACHE_CHECK'      =>  false,   // 数据缓存是否校验缓存
    'DATA_CACHE_PREFIX'     =>  '',     // 缓存前缀
    'DATA_CACHE_TYPE'       =>  'File',  // 数据缓存类型,支持:File|Db|Apc|Memcache|Shmop|Sqlite|Xcache|Apachenote|Eaccelerator
    'DATA_CACHE_PATH'       =>  TEMP_PATH,// 缓存路径设置 (仅对File方式缓存有效)
    'DATA_CACHE_SUBDIR'     =>  true,    // 使用子目录缓存 (自动根据缓存标识的哈希创建子目录)
    'DATA_PATH_LEVEL'       =>  3,       // 子目录缓存级别
    
    'COOKIE_EXPIRE'         =>  0,    // Cookie有效期
    'COOKIE_PATH'           =>  '/',     // Cookie路径
    'COOKIE_PREFIX'         =>  'e_',      // Cookie前缀 避免冲突
    'COOKIE_HTTPONLY'       =>  'true',      // Cookie httponly设置
    
//  'LOG_RECORD'            =>  TRUE,   // 默认不记录日志
//  'LOG_TYPE'              =>  'File', // 日志记录类型 默认为文件方式
//  'LOG_LEVEL'             =>  'EMERG,ALERT,CRIT,ERR,WARN,NOTICE,INFO,DEBUG,SQL',// 允许记录的日志级别
//  'LOG_LEVEL'             =>  'ERR,WARN,NOTICE',// 允许记录的日志级别
//  'LOG_FILE_SIZE'         =>  2097152,	// 日志文件大小限制
//  'LOG_EXCEPTION_RECORD'  =>  true,    // 是否记录异常信息日志
    
    'AUTOLOAD_NAMESPACE' => array(    //自动加载的命名空间
    	'Lib'     => APP_PATH.'Lib',    //用户自定义函数库文件夹
	),
);