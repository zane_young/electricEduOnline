<?php
namespace Lib;
/**
 * ligerUI的分页类
 */
class LigerPage {
	public $pageNum = 1;
	public $pageSize = 20;
	public $orderBy = 'id desc';
	public $where = 'id desc';
	public $totalRows = 0;
	public $totalPages = 0;
	public $firstRow = 0;
	function __construct() {
		$post = I('post.');
		$this->pageNum = $post['page'];
		$this->pageSize = $post['pagesize'];
		$this->orderBy = $post['orderBy'];
		unset($post['pageNum']);
		unset($post['pageSize']);
		unset($post['orderBy']);

		$where = array();
		foreach ($post as $key => $value) {
			$fieldLen = strlen($key) - 4;
			if (strpos($key, '_exp') === $fieldLen) {
				$field = substr($key, 0, $fieldLen);
				if($post[$field]!==''){
					if ($value === 'like') {
						$post[$field] = '%' . $post[$field] . '%';
					}
					$where[$field] = array($value, $post[$field]);
				}
			}
		}
		$this->where=$where;
		\Lib\Util::tplog($where);
	}
	public function init($totalRows){
		$this->totalRows=$totalRows;
		$this->firstRow=($this->pageNum-1)*$this->pageSize;
		$this->totalPages=ceil($totalRows/$this->pageSize);
	}
}
