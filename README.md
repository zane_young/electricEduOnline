#electricEduOnline 

##1.目录说明:
|-criterion 规范(暂时不用)    
|-database 数据库设计文档(命名规范      xxxxx项目数据库设计文档_<编写者姓名>.doc)以及sql文件    
|-docdemos 文档模版目录     
|-docs 需求文档等其他文档目录     
|-reference 引用文件、第三方的框架、工具程序等        
|-ui 美工设计的psd和图片等     

##2.开发规范说明:
* 所有开发者的本地数据库的帐号和密码均为yun(本地mysql新建用户，避免root用户密码不一致)
* 所有源码文件编码均为utf-8
* 数据库名为eeodb,编码为utf-8
* 数据库表前缀统一为eeo_
* 对象，变量和函数命名均采用php默认的规范

##3.开发工具说明:
* 前端+后台php均采用huilder    
* 官网: <a href="http://www.dcloud.net.cn/" target="_blank">链接</a>
* 注:aptana插件php代码提示设置:
![图片](http://images.cnitblog.com/blog/469519/201408/262156160323949.png)

##4.框架说明:
* [thinkphp3.2.2](http://www.thinkphp.cn/)
* [后台模版dwz](http://www.j-ui.com/)
* [phpexcel](http://phpexcel.codeplex.com/)
* [ie6不支持svg统计图的解决方案](http://answers.microsoft.com/zh-hans/ie/forum/ie8-windows_other/svg%E5%9B%BE%E5%BD%A2%E4%B8%8D%E6%94%AF%E6%8C%81/d2f47d51-723d-41bf-afb1-d2fdfea0ff09)
* [ie6绿色版  提取码：fe9a](http://yunpan.cn/Q7mppncetf9WU)
