<?php exit;?>DROP TABLE IF EXISTS yun_admin
CREATE TABLE `yun_admin` (  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,  `groupid` tinyint(4) NOT NULL DEFAULT '1',  `username` char(10) NOT NULL,  `realname` char(15) NOT NULL,  `password` char(32) NOT NULL,  `lastlogin_time` int(10) unsigned NOT NULL,  `lastlogin_ip` char(15) NOT NULL,  `iflock` tinyint(1) unsigned NOT NULL DEFAULT '0',  PRIMARY KEY (`id`),  UNIQUE KEY `usename` (`username`),  KEY `groupid` (`groupid`)) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='管理员信息表';
INSERT INTO yun_admin VALUES('1','1','admin','yunstudio','168a73655bfecefdb15b14984dd2ad60','1394360780','unknown','0')
DROP TABLE IF EXISTS yun_fragment
CREATE TABLE `yun_fragment` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `title` varchar(255) NOT NULL,  `sign` varchar(255) NOT NULL COMMENT '前台调用标记',  `content` text NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO yun_fragment VALUES('1','右侧公告信息','announce','<p>\r\n	本站为Yuncms的默认演示模板，Yuncms是一款基于PHP+MYSQL构建的高效网站管理系统。 后台地址请在网址后面加上/index.php?yun=admin进入。 后台的用户名:admin;密码:123456，请进入后修改默认密码。\r\n</p>\r\n<p>\r\n	<img src=\"/yuncms/upload/fragment/image/20140224/20140224192956_37828.jpg\" width=\"100\" height=\"120\" alt=\"\" /> \r\n</p>')
DROP TABLE IF EXISTS yun_group
CREATE TABLE `yun_group` (  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(255) NOT NULL,  `power` varchar(1000) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO yun_group VALUES('1','超级管理员','-1')
INSERT INTO yun_group VALUES('2','普通管理员','277,283,1,2,4,5,6,7,8,9,228,10,11,12,13,14,15,16,157,158,174,268,288')
INSERT INTO yun_group VALUES('3','test','277,283,1,2,4,5,6,7,8,9,228')
DROP TABLE IF EXISTS yun_link
CREATE TABLE `yun_link` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `type` tinyint(1) NOT NULL COMMENT '类型',  `norder` int(5) NOT NULL COMMENT '排序',  `name` varchar(30) NOT NULL COMMENT '站点名',  `url` varchar(40) NOT NULL COMMENT '站点地址',  `picture` varchar(30) NOT NULL COMMENT '本地logo',  `logourl` varchar(50) NOT NULL COMMENT '远程logo',  `siteowner` varchar(30) NOT NULL COMMENT '站点所有者',  `info` varchar(300) NOT NULL COMMENT '介绍',  `ispass` tinyint(1) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO yun_link VALUES('2','2','0','云作坊','http://www.yunstudio.net','1342232581.png','','云作坊','','1')
INSERT INTO yun_link VALUES('6','1','0','科技交流平台','http://www.kjjlpt.com','','','','','1')
DROP TABLE IF EXISTS yun_member_group
CREATE TABLE `yun_member_group` (  `id` int(3) NOT NULL AUTO_INCREMENT,  `name` varchar(30) NOT NULL,  `notallow` text NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO yun_member_group VALUES('2','普通会员','yun')
INSERT INTO yun_member_group VALUES('4','超级会员','')
DROP TABLE IF EXISTS yun_members
CREATE TABLE `yun_members` (  `id` int(20) NOT NULL AUTO_INCREMENT,  `groupid` int(3) NOT NULL,  `account` varchar(30) NOT NULL,  `password` varchar(60) NOT NULL,  `rmb` int(8) NOT NULL DEFAULT '0',  `crmb` int(8) NOT NULL DEFAULT '0',  `nickname` varchar(30) NOT NULL,  `email` varchar(30) NOT NULL,  `tel` varchar(15) NOT NULL,  `qq` varchar(20) NOT NULL,  `regtime` int(11) NOT NULL,  `regip` varchar(16) NOT NULL,  `lasttime` int(11) NOT NULL,  `lastip` varchar(16) NOT NULL,  `islock` tinyint(4) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO yun_members VALUES('1','4','admin','d707c24bd27660ca7d65870027fb9218','9000','3774','会员演示','404138@qq.com','13638816362','404133749','0','','1394343731','unknown','0')
INSERT INTO yun_members VALUES('2','2','yunstudio','663d82c90c57ffa5005b4a1a0911b391','0','0','','yunstudio2012@qq.com','','','1372135503','unknown','1372135503','unknown','0')
INSERT INTO yun_members VALUES('3','2','nimei','6857d1c563b6217fb797453f467a1dbc','0','0','','yunstudio2012@qq.com','','','1373010733','unknown','1373619128','unknown','0')
DROP TABLE IF EXISTS yun_method
CREATE TABLE `yun_method` (  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,  `rootid` int(10) unsigned NOT NULL,  `pid` float unsigned NOT NULL,  `operate` varchar(255) NOT NULL,  `name` varchar(255) NOT NULL,  `ifmenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否菜单显示',  PRIMARY KEY (`id`),  KEY `pid` (`pid`)) ENGINE=MyISAM AUTO_INCREMENT=317 DEFAULT CHARSET=utf8;
INSERT INTO yun_method VALUES('1','1','0','admin','后台登陆管理','1')
INSERT INTO yun_method VALUES('2','1','1','index','管理员管理','1')
INSERT INTO yun_method VALUES('4','1','1','admindel','管理员删除','0')
INSERT INTO yun_method VALUES('5','1','1','adminedit','管理员编辑','0')
INSERT INTO yun_method VALUES('6','1','1','adminlock','管理员锁定','0')
INSERT INTO yun_method VALUES('7','1','1','group','权限管理','1')
INSERT INTO yun_method VALUES('8','1','1','groupedit','管理组编辑','0')
INSERT INTO yun_method VALUES('9','1','1','groupdel','管理组删除','0')
INSERT INTO yun_method VALUES('10','10','0','news','资讯管理','1')
INSERT INTO yun_method VALUES('11','10','10','index','已有资讯','1')
INSERT INTO yun_method VALUES('12','10','10','add','添加资讯','1')
INSERT INTO yun_method VALUES('13','10','10','edit','资讯编辑','0')
INSERT INTO yun_method VALUES('14','10','10','del','资讯删除','0')
INSERT INTO yun_method VALUES('15','10','10','lock','资讯锁定','0')
INSERT INTO yun_method VALUES('16','10','10','recmd','资讯推荐','0')
INSERT INTO yun_method VALUES('17','17','0','dbback','数据库管理','1')
INSERT INTO yun_method VALUES('18','17','17','index','数据库备份','1')
INSERT INTO yun_method VALUES('19','17','17','recover','备份恢复','0')
INSERT INTO yun_method VALUES('20','17','17','detail','备份详细','0')
INSERT INTO yun_method VALUES('21','17','17','del','备份删除','0')
INSERT INTO yun_method VALUES('22','22','0','index','后台面板','0')
INSERT INTO yun_method VALUES('23','22','22','index','后台首页','0')
INSERT INTO yun_method VALUES('24','22','22','login','登陆','0')
INSERT INTO yun_method VALUES('25','22','22','logout','退出登陆','0')
INSERT INTO yun_method VALUES('26','22','22','verify','验证码','0')
INSERT INTO yun_method VALUES('27','22','22','welcome','服务器环境','0')
INSERT INTO yun_method VALUES('28','28','0','set','全局设置','1')
INSERT INTO yun_method VALUES('29','28','28','index','网站设置','1')
INSERT INTO yun_method VALUES('30','30','0','sort','分类管理','1')
INSERT INTO yun_method VALUES('31','30','30','index','栏目列表','1')
INSERT INTO yun_method VALUES('33','30','30','del','分类删除','0')
INSERT INTO yun_method VALUES('277','0','0','appmanage','应用管理','1')
INSERT INTO yun_method VALUES('85','28','28','menuname','后台功能','1')
INSERT INTO yun_method VALUES('159','150','150','images_upload','图片批量上传','0')
INSERT INTO yun_method VALUES('158','10','10','FileManagerJson','编辑器上传管理','0')
INSERT INTO yun_method VALUES('157','10','10','UploadJson','编辑器上传','0')
INSERT INTO yun_method VALUES('174','10','10','cutcover','封面图剪切','0')
INSERT INTO yun_method VALUES('236','30','30','PageUploadJson','单页上传','0')
INSERT INTO yun_method VALUES('235','30','30','pageedit','单页编辑','0')
INSERT INTO yun_method VALUES('234','30','30','pageadd','添加单页栏目','0')
INSERT INTO yun_method VALUES('231','30','30','newsedit','文章栏目编辑','0')
INSERT INTO yun_method VALUES('230','30','30','newsadd','添加文章栏目','0')
INSERT INTO yun_method VALUES('182','28','28','clear','网站缓存','1')
INSERT INTO yun_method VALUES('188','188','0','link','友情链接','1')
INSERT INTO yun_method VALUES('189','188','188','index','链接列表','1')
INSERT INTO yun_method VALUES('190','188','188','add','添加链接','1')
INSERT INTO yun_method VALUES('191','188','188','edit','链接编辑','0')
INSERT INTO yun_method VALUES('192','188','188','del','链接删除','0')
INSERT INTO yun_method VALUES('228','1','1','adminnow','账户管理','1')
INSERT INTO yun_method VALUES('229','188','188','lock','锁定','0')
INSERT INTO yun_method VALUES('237','30','30','PageFileManagerJson','单页上传管理','0')
INSERT INTO yun_method VALUES('238','238','0','fragment','碎片管理','1')
INSERT INTO yun_method VALUES('239','238','238','index','碎片列表','1')
INSERT INTO yun_method VALUES('240','238','238','add','碎片添加','1')
INSERT INTO yun_method VALUES('241','238','238','edit','碎片编辑','0')
INSERT INTO yun_method VALUES('242','238','238','del','碎片删除','0')
INSERT INTO yun_method VALUES('243','238','238','UploadJson','编辑器上传','0')
INSERT INTO yun_method VALUES('244','238','238','FileManagerJson','编辑器上传管理','0')
INSERT INTO yun_method VALUES('245','28','28','tpchange','前台模板','1')
INSERT INTO yun_method VALUES('251','30','30','pluginadd','添加应用栏目','0')
INSERT INTO yun_method VALUES('252','30','30','pluginedit','应用栏目编辑','0')
INSERT INTO yun_method VALUES('267','258','258','file','文件上传','0')
INSERT INTO yun_method VALUES('288','10','10','colchange','资讯转移栏目','0')
INSERT INTO yun_method VALUES('283','0','0','member','会员管理(应用)','1')
INSERT INTO yun_method VALUES('292','28','28','tplist','模板文件列表','0')
INSERT INTO yun_method VALUES('293','28','28','tpadd','模板文件添加','0')
INSERT INTO yun_method VALUES('294','28','28','tpedit','模板文件编辑','0')
INSERT INTO yun_method VALUES('295','28','28','tpdel','删除模板文件','0')
INSERT INTO yun_method VALUES('296','28','28','tpgetcode','获取模板内容','0')
INSERT INTO yun_method VALUES('301','30','30','add','添加栏目','1')
INSERT INTO yun_method VALUES('304','30','30','placelist','内容定位列表','1')
INSERT INTO yun_method VALUES('305','30','30','placeadd','添加内容定位','1')
INSERT INTO yun_method VALUES('306','30','30','placeedit','定位编辑','0')
INSERT INTO yun_method VALUES('307','30','30','placedel','定位删除','0')
INSERT INTO yun_method VALUES('308','308','0','tags','TAG标签','1')
INSERT INTO yun_method VALUES('309','308','308','index','标签列表','1')
INSERT INTO yun_method VALUES('310','308','308','del','删除标签','0')
INSERT INTO yun_method VALUES('311','308','308','hits','编辑点击量','0')
INSERT INTO yun_method VALUES('312','308','308','add','生成标签','1')
INSERT INTO yun_method VALUES('313','308','308','mesup','文档数量更新','0')
INSERT INTO yun_method VALUES('314','314','0','files','附件管理','1')
INSERT INTO yun_method VALUES('315','314','314','index','文件列表','1')
INSERT INTO yun_method VALUES('316','314','314','del','删除文件','0')
DROP TABLE IF EXISTS yun_news
CREATE TABLE `yun_news` (  `id` int(20) NOT NULL AUTO_INCREMENT,  `sort` varchar(350) NOT NULL COMMENT '类别',  `account` char(15) NOT NULL COMMENT '发布者账户',  `title` varchar(60) NOT NULL COMMENT '标题',  `places` varchar(100) NOT NULL,  `color` varchar(7) NOT NULL COMMENT '标题颜色',  `picture` varchar(80) NOT NULL,  `keywords` varchar(300) NOT NULL COMMENT '关键字',  `description` varchar(600) NOT NULL,  `content` text NOT NULL COMMENT '内容',  `method` varchar(100) NOT NULL COMMENT '方法',  `tpcontent` varchar(100) NOT NULL COMMENT '模板',  `norder` int(4) NOT NULL COMMENT '排序',  `recmd` tinyint(1) NOT NULL COMMENT '推荐',  `hits` int(10) NOT NULL COMMENT '点击量',  `ispass` tinyint(1) NOT NULL,  `origin` varchar(30) NOT NULL COMMENT '来源',  `addtime` int(11) NOT NULL,  `extfield` int(10) NOT NULL DEFAULT '0' COMMENT '拓展字段',  PRIMARY KEY (`id`),  FULLTEXT KEY `sort` (`sort`)) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
INSERT INTO yun_news VALUES('23',',000000,100028,100036','admin','激情','100','#E53333','20140309/thumb_20140309144706_30440.jpg','哈哈','哈哈','哈','news/content','news_content','0','0','30','1','原创','1394346935','0')
INSERT INTO yun_news VALUES('24',',000000,100028,100036','admin','果敢','100','#00D5FF','20140309/thumb_thumb_20140309144918_26526.jpg','果敢','果敢','果敢<img src=\"/yuncms/upload/news/image/20140309/20140309151316_19060.jpg\" alt=\"\" /><img src=\"/yuncms/upload/news/image/20140309/20140309151316_71083.jpg\" alt=\"\" />','news/content','news_content','0','0','31','1','果敢','1394347730','0')
INSERT INTO yun_news VALUES('25',',000000,100028,100036','admin','执着','100','','20140309/thumb_thumb_20140309183130_90592.jpg','执着','执着','执着','news/content','news_content','0','0','31','1','原创','1394361075','0')
INSERT INTO yun_news VALUES('26',',000000,100028,100036','admin','超越','100','#B8D100','20140309/thumb_20140309183155_88695.jpg','超越','超越','超越','news/content','news_content','0','0','30','1','原创','1394361101','0')
DROP TABLE IF EXISTS yun_page
CREATE TABLE `yun_page` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `sort` varchar(350) NOT NULL,  `content` text NOT NULL,  `edittime` varchar(20) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO yun_page VALUES('3',',000000,100033','yuncms','2014-02-27 14:28:57')
DROP TABLE IF EXISTS yun_place
CREATE TABLE `yun_place` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `name` varchar(60) NOT NULL,  `norder` int(5) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
INSERT INTO yun_place VALUES('100','首页banner','0')
INSERT INTO yun_place VALUES('101','首页幻灯','0')
DROP TABLE IF EXISTS yun_sort
CREATE TABLE `yun_sort` (  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '模型类别',  `path` varchar(255) DEFAULT NULL,  `name` varchar(255) DEFAULT NULL,  `deep` int(5) unsigned NOT NULL DEFAULT '1' COMMENT '深度',  `norder` tinyint(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',  `ifmenu` tinyint(1) NOT NULL COMMENT '是否前台显示',  `method` varchar(100) NOT NULL COMMENT '模型方法',  `tplist` varchar(100) NOT NULL COMMENT '列表模板',  `keywords` varchar(255) NOT NULL COMMENT '描述',  `description` varchar(300) NOT NULL COMMENT '描述',  `url` varchar(100) NOT NULL COMMENT '外部链接',  `extendid` int(10) DEFAULT NULL COMMENT '拓展表id',  PRIMARY KEY (`id`),  FULLTEXT KEY `path` (`path`)) ENGINE=MyISAM AUTO_INCREMENT=100037 DEFAULT CHARSET=utf8;
INSERT INTO yun_sort VALUES('100028','1',',000000','新闻资讯','1','0','1','news/index','news_index,news_content','资讯信息','资讯信息','10','0')
INSERT INTO yun_sort VALUES('100029','1',',000000,100028','最新动态','2','0','1','news/index','news_index,news_content','最新动态','最新动态','10','0')
INSERT INTO yun_sort VALUES('100033','3',',000000','关于我们','1','0','1','page/index','page_index','关于我们','关于我们','','')
INSERT INTO yun_sort VALUES('100034','1',',000000,100028,100029','最近公告','3','0','1','news/index','news_index,news_content','最近公告','最近公告','10','0')
INSERT INTO yun_sort VALUES('100036','1',',000000,100028','作坊文化','2','0','0','news/index','news_index,news_content','作坊文化','作坊文化','10','0')
DROP TABLE IF EXISTS yun_tags
CREATE TABLE `yun_tags` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `name` varchar(60) NOT NULL,  `hits` int(10) NOT NULL DEFAULT '0',  `mesnum` int(10) NOT NULL DEFAULT '0',  `addtime` int(11) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
