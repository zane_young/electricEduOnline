function review (obj) {
	for (var i in obj) {
		$('input[name='+i+']').val(obj[i]);
		$('input[type=checkbox][name='+i+']').attr('checked',true);
		$('input[type=radio][name='+i+']').attr('checked',true);
		$('select[name='+i+'] option[value='+obj[i]+']').attr('selected',true);
	}
}