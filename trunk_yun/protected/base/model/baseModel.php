<?php
/*
 * 模型基类
 * */
class baseModel extends model{
     protected $prefix='';
	 public function getPre(){
	 	return $this->prefix;
	 }
	 public function getTable(){
	 	return $this->prefix;
	 }
	 public function getFullTable(){
	 	return $this->prefix.$this->table;
	 }
     public function __construct( $database= 'DB' ){
		parent::__construct();
		$this->prefix=config('DB_PREFIX');
	}
}