<?php
/*
 * 模型父类，数据库操作函数的父类，模型最低层的类
 * */
class model{
	public $model = NULL;
	protected $db = NULL;
	protected $table = "";//表名
	protected $ignoreTablePrefix = false;//是否忽视表的前缀
	
	public function __construct( $database= 'DB', $force = false){
		$this->model = self::connect( config($database), $force);
		$this->db = $this->model->db;
	}
	
	//连接数据库
	static public function connect($config, $force=false){
		static $model = NULL;
		if( $force==true || empty($model) ){
			$model = new cpModel($config);
		}
		return $model;
	}
	
	//执行sql语句
	public function query($sql,$params = array()){
		return $this->model->query($sql,$params);
	}
	
	//找到符合当前添加的记录，返回数组形式
	public function find($condition = '', $field = '', $order = ''){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->field($field)->where($condition)->order($order)->find();
	}
	
	public function select($condition = '', $field = '', $order = '', $limit = ''){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->field($field)->where($condition)->order($order)->limit($limit)->select();
	}
	
	public function count($condition = ''){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->where($condition)->count();
	}
	
	public function insert($data = array() ){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->data($data)->insert();
	}
	//更新数据
	public function update($condition, $data = array() ){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->data($data)->where($condition)->update();
	}
	
	public function delete($condition){
		return $this->model->table($this->table, $this->ignoreTablePrefix)->where($condition)->delete();
	}
	
	public function getSql(){
		return $this->model->getSql();
	}
	
	public function escape($value){
		return $this->model->escape($value);
	}
	
	public function cache($time = 0){
		$this->model->cache($time);
		return $this;
	}
	
}