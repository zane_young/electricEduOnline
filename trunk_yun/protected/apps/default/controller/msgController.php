<?php
class msgController extends loginCheckController {
	public function index() {
		$msgModel=model('msg');
		$msgModel=new msgModel;
		$key=in($_GET['key']);

		$listRows=empty($_GET['pageSize'])?10:intval($_GET['pageSize']);//每页显示的信息条数,默认每页十条
		$url=url('msg/index',array('page'=>'{page}'));
	    $limit=$this->pageLimit($url,$listRows);

		$list=array();
		$pre=$msgModel->getPre();
		$table=$msgModel->getFullTable();
		if(!empty($key)){
			$where="title like '%{$key}%' or content like '%{$key}%'";
		}
		$count=$msgModel->count($where);	
		if($where){
			$where='where '.$where.' and parentid=0';
		}else{
			$where='where parentid=0';
		}
		$list=$msgModel->query("select id,name,title,content,parentid,islock from {$table} {$where} order by id desc limit {$limit}");
		if(is_array($list)){
			for ($i=0,$len=count($list); $i < $len; $i++) { 
				$msg=$list[$i];
				$replyList=$msgModel->query("select id,name,title,content,islock from {$table} where parentid={$msg['id']} order by id desc");
				if(!$replyList) $replyList=array();
				$list[$i]['replyList']=$replyList;
			}
		}
		$this->alist=$list;
		$this->page=$this->pageShow($count);
		
		$this->display();
	}
	
	public function add(){
		$this->checkLogin();
		$msg['addtime']=date('Y-m-d H:i:s',time());
		
		$replyId=$_POST['replyId'];
		if($replyId&&$replyId=intval($replyId)){
			$msg['parentid']=intval($replyId);
		}
		
		$msg['content']=in($_POST['content']);
		$msg['account']=$this->auth['account'];
		$msg['name']=$this->auth['nickname'];
		$msgModel=model('msg');
		$msgModel=new msgModel;
		$msgModel->insert($msg);
		if(isset($msg['parentid'])){
			$this->success('回复成功!');	
		}else{
			$this->success('发表问题成功!');	
		}
	}
}
