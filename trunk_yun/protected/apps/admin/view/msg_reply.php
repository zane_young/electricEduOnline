<?php if(!defined('APP_NAME')) exit;?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="__PUBLIC__/admin/css/back.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="__PUBLIC__/js/jquery.js"></script>
<script	language="javascript">
  $(function ($) { 
	//行颜色效果
	$('.all_cont tr').hover(
	function () {
        $(this).children().css('background-color', '#f9f9f9');
	},
	function () {
        $(this).children().css('background-color', '#fff');
	}
	);	
  });
</script>
<title>{$t_name}会员组</title>
</head>
<body>
<div class="contener">
<div class="list_head_m">
		<div class="list_head_ml">你当前的位置：【回复编辑】</div>
		<div class="list_head_mr"></div>
		</div>

		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="all_cont">
			<form action="" method="post" id="info" name="info">
            <tr>
               <td align="right">提问人：</td>
               <td>{$question['name']}</td>
               <td class="inputhelp"></td>
            </tr>
            <tr>
               <td align="right">问题：</td>
               <td>{$question['content']}</td>
               <td class="inputhelp"></td>
            </tr>
            <tr>
               <td align="right">回复</td>
               <td><textarea name="content" cols="60" rows="10">{$info['content']}</textarea></td>
               <td class="inputhelp"></td>
            </tr>
			<tr>
				<td width="200">&nbsp;</td>
				<td align="left" colspan="2"><input type="hidden" name="id" value="{$_GET['id']}"> <input type="submit" value="回复" class="btn btn-primary btn-small"></td>
			</tr>
			</form>
		</table>
        </div>
</body>
</html>