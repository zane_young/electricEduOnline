<?php if(!defined('APP_NAME')) exit;?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="__PUBLIC__/admin/css/back.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="__PUBLIC__/js/jquery.js"></script>
<script type="text/javascript" src="__PUBLIC__/js/common.js"></script>
<script language="javascript">
function CheckAll(form) { //复选框全选/取消
	for (var i=0;i<form.elements.length;i++) { 
		var e = form.elements[i]; 
		if (e.Name != "chkAll"&&e.disabled!=true) 
		e.checked = form.chkAll.checked; 
	} 
  } 

function lock(obj){
	     obj.click(function(){
			var nowobj=$(this);
			var id=nowobj.parent().parent().attr('id');
			$.post("{url('msg/lock')}", {id:id,islock:1},
   				function(data){
					if(data==1){
                      nowobj.html("解冻");
					  nowobj.attr('class','unlock');
					  nowobj.unbind("click");
					  unlock(nowobj);
					}else{
						alert(data);
					} ;
   			});
		});
}

function unlock(obj){
		obj.click(function(){
			var nowobj=$(this);
			var id=nowobj.parent().parent().attr('id');
			
			$.post("{url('msg/lock')}", {id:id,islock:0},
   				function(data){
					if(data==1){
                      nowobj.html("冻结");
					  nowobj.attr('class','lock');
					  nowobj.unbind("click");
					  lock(nowobj);
					}else{
						 alert(data);
					};
   			});
		});
}
$(function ($) { 
	//行颜色效果
	$('.all_cont tr').hover(
	function () {
        $(this).children().css('background-color', '#f9f9f9');
	},
	function () {
        $(this).children().css('background-color', '#fff');
	}
	);
	lock($('.lock'));
	unlock($('.unlock'));
	
	//ajax操作
	 $('.del').click(function(){
			if(confirm('删除将不可恢复~')){
			var delobj=$(this).parent().parent();
			var id=delobj.attr('id');
			$.get("{url('msg/del')}", {id:id},
   				function(data){
					if(data==1){
                      	delobj.remove();
					}else{
						 alert(data);
					}
   			});
			}
	  });
	  function setParam(url,param) {  
		    if (url.indexOf("?") > 0)  
		        return url + "&" +param;  
		    else  
		        return url + "?" + param;  
		}  
	  
	  //数据回显
//	  $("select option[value={$_GET['key']}]").attr('checked',true);
		
		review({json_encode($_GET)});
  });
</script>
<title>答疑列表</title>
</head>
<body>
<div class="contener">
<div class="list_head_m">
           <div class="list_head_ml">你当前的位置：【答疑列表】 <a href="{url('default/msg/index')}" target="_blank">查看前台</a></div>
           <div class="list_head_mr">
           </div>
        </div>
         <table width="100%" border="0" cellpadding="0" cellspacing="1"   class="all_cont" >
         <tr>
            <td colspan="7" align="left">
               <form action="{url('msg/index')}" method="GET" >
                 <div style="float:left"> 搜索：
                  <select name="stype">
                     <option value="1">内容</option>
                     <option value="2">账户</option>
                     <option value="3">姓名</option>
                  </select>
                  </div>
                 <div style="float:left"> <input type="text" name="key" size="20" value="{$_GET['key']}"> </div>
                  <input name="yun" type="hidden" value="{$_GET['yun']}" /><!--get[yun]就是当前的页面方法-->
                  <div style="float:left"><input class="btn btn-success  btn-small" type="submit" value="搜索"></div>
               </form> 
          </tr>
         <form action="{url('msg/del')}" method="post" onSubmit="return confirm('删除不可以恢复~确定要删除吗？');"> 
          <tr>
              <th align="center" width="70"><input type="checkbox" name="chkAll" value="checkbox" onClick="CheckAll(this.form)"/></th>
              <th width="10%">账户</th>
              <th width="10%">姓名</th>
              <th width="40%">内容</th>
              <th width="10%">发布时间</th>
              <th width="10%">类型</th>
              <th width="14%">管理选项</th>
          </tr>
          <?php 
              if(!empty($list)){
                   foreach($list as $vo){
                     $book.='<tr id="'.$vo['id'].'"><td align="center"><input type="checkbox" name="delid[]" value="'.$vo['id'].'" /></td><td align="center">';
                     $book.=$vo['account'].'</td><td align="center">';
                     $book.=$vo['name'].'</td><td align="center">';
                     $book.=(strlen($vo['content'])>40?substr($vo['content'],0,40):$vo['content']).'</td><td align="center">';
                     $book.=$vo['addtime'].'</td><td align="center">';
                     $book.=($vo['parentid']>0?'<div>回复</div>':'<div>提问</div>').'</td><td align="center">';
                     $book.=($vo['parentid']>0?'':'<div class="edt"><a href="'.url('msg/reply',array('id'=>$vo['id'])).'">回复</a></div>');
                     $book.=$vo['islock']?'<div class="unlock">解冻 </div>':'<div class="lock">冻结 </div>';
                     $book.='<div class="del">删除</div></td></tr>';
                    } 
                   echo $book;
               }               
            ?>   
            <tr>
             <td align="center"><input type="submit" class="btn btn-small"  value="删除"></td>
             <td colspan="8"><div class="pagelist">{$page}</div></td>
          </tr>
          </form>  
        </table>
  </div>
</body>
</html>