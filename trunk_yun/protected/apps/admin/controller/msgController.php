<?php
/*
 * 后台答疑管理控制器
 * */
class msgController extends commonController
{
	//在线答疑列表
	public function index()
	{
	 	$msgModel=model('msg');
//		$msgModel=new msgModel;
		$stype=in($_GET['stype']);
		$key=in($_GET['key']);

		$listRows=empty($_GET['pageSize'])?10:intval($_GET['pageSize']);//每页显示的信息条数,默认每页十条
		$url=url('msg/index',array('page'=>'{page}'));
	    $limit=$this->pageLimit($url,$listRows);

		$list=array();
		$pre=$msgModel->getPre();
		$table=$msgModel->getFullTable();
		$count=0;
		if(!empty($key)){
			$stype=intval($stype);
			if($stype===1){
				$where="where title like '%{$key}%' or content like '%{$key}%'";
				$count=$msgModel->query("select count(id) as count from {$table} {$where}");
				$count=$count[0]['count'];
			}else if($stype===2){
				$where="where account like '%{$key}%'";
				$count=$msgModel->query("select count(id) as count from {$table} {$where}");
				$count=$count[0]['count'];
			}else if($stype===3){
				$where="where name like '%{$key}%'";
				$count=$msgModel->query("select count(id) as count from {$table} {$where}");
				$count=$count[0]['count'];
			}
		}
		$list=$msgModel->query("select name,account,id,title,content,parentid,islock,addtime from {$table} {$where} order by id desc limit {$limit}");
		$this->list=$list;
		$this->page=$this->pageShow($count);
		
		$this->display();
	}
	
	
	//管理员删除，通过传参数过来删除
	public function del()  
	{
	 	$msgModel=model('msg');
//		$msgModel=new msgModel;
		$table=$msgModel->getFullTable();
		$pre=$msgModel->getPre();
		if($this->isPost()){
			$ids=$_POST['delid'];
			if(is_array($ids)){
				if(count($ids)===0){
					$this->error('请至少选择一项');
				}else{
					$idsStr=implode(',', $ids);
				}
			}else{
				$idsStr=intval($ids);
			}
			$re=$msgModel->query("delete from {$table} where id in ({$idsStr})");
			if($re!==FALSE){
				$this->success('删除成功');
			}else{
				$this->error('删除失败');
			}
		}else{
			$id=intval($_GET['id']);
			if(empty($id)) $this->error('参数错误');
			if($msgModel->query("delete from {$table} where id = {$id}")!==FALSE){
				echo 1;
			}else{
				echo '删除失败';
			}
			
		}
	}
	

	//管理员锁定，通过传递参数
	public function lock() 
	{
		$id=intval($_POST['id']);
		$lock['islock']=intval($_POST['islock']);
		if(empty($id)) $this->error('非法操作~');
		if($lock['islock']===0 || $lock['islock']===1){
			model('msg')->update("id='{$id}'",$lock);
			echo 1; 
		}else{
			echo '修改失败~';
		}
	}
	
	public function reply(){
		if($this->isPost()){
			$id=intval($_POST['id']);
			if(!$id){
				$this->error('参数错误~');
			}
			$content=in($_POST['content']);
			if(empty($content)){
				$this->error('回复不能为空~');
			}
			
		 	$msgModel=model('msg');
			$msgModel=new msgModel;
			$msg['parentid']=$id;
			$msg['content']=$content;
			$msg['name']=$_SESSION['admin_realname'];
			$msg['account']=$_SESSION['admin_username'].'(管理员)';
			$msg['addtime']=date('Y-m-d H:i:s',time());
			if($msgModel->insert($msg)){
				$this->success('回复成功');	
			}
			$this->success('回复失败');
		}else{
			$id=intval($_GET['id']);
			if($id<=0){
				$this->error('参数错误');
			}
			$this->question=model('msg')->find("id=$id");
			$this->display();
		}
	}
}