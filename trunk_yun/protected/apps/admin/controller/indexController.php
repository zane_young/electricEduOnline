<?php
/*
 * 后台首页的控制器
 * */
class indexController extends commonController
{
	public function __construct()
	{
		parent::__construct();
		//print_r(Auth::getModule());//遍历所有模型和方法初始method表中数据
	}
	
	//显示管理后台首页
	public function index()
	{
		/*
		 * 后台通过auth类进行验证，完美结合！
		 * 经过auth处理后的groupid
		 * Auth::$config['AUTH_SESSION_PREFIX'].'groupid'; 为auth_groupid
		 * $_SESSION['auth_groupid']是通过auth保存
		 * */
		$groupid=$_SESSION[Auth::$config['AUTH_SESSION_PREFIX'].'groupid'];
		$power=model('group')->find("id = {$groupid}",'power');//权限组
		//power=-1,系统最高管理员   ifmenu=1获取后台导航菜单
		if($power['power']==-1) $where="ifmenu = '1'";
		//不同权限的管理员检索对应的权限
		else $where=/*ifmenu = '1' AND */"id IN(".$power['power'].")";
		//显示所拥有权限的栏目
		$methods=model('method')->select($where,'','rootid,id');
		
		//构造菜单,这一块比较复杂,原理就是查询methods表，然后进行分类，注意对拓展应用的单独管理
		if(!empty($methods)){
			$operate='';
			$page=array();
			$pluginlist=array();//拓展应用所有操作的数组
			foreach ($methods as $vo){
				if($vo['pid']==0){
					//pid=0,是父级栏目,是各个应用
					$operate=$vo['operate'];
//					echo $operate."<br>";
					$root[$operate]['channel']=$vo['name']?$vo['name']:$vo['operate'];//各个应用的名称
					//echo $root[$operate]['channel']."<br>";
					$root[$operate]['pages']=array();
					
					if($vo['rootid']==0){
					//rootid=0 是拓展应用，包括应用管理和会员管理
						//getadminMenu 函数读取拓展应用下面的栏目
						$plugmenu=api($operate,'getadminMenu');//通过api文件获取拓展应用菜单,包括应用管理和会员管理
						//print_r($plugmenu);
						if(is_array($plugmenu)){
						   $pluginlist[]=$operate;
						   $root[$operate]['pages']=$plugmenu;//拓展应用数组
						}
						//print_r($root[$operate]['pages']);
					   }
				}else{
					//非拓展应用，即其他普通应用，凑出操作函数
					$page['name']=$vo['name']?$vo['name']:$vo['operate'];
					$page['url']=url($operate.'/'.$vo['operate']);
					$root[$operate]['pages'][]=$page;
					//print_r($page);
				}
			}
		}else $this->error('获取后台导航菜单失败~');
		$menu=Array(
		   Array('title'=>'管理首页','channels' => Array($root['set'],$root['admin'],$root['dbback'],$root['files'])),//默认显示菜单
		   Array('title'=>'结构管理','channels' => Array($root['sort'],$root['fragment'])),
		   Array('title'=>'内容管理','channels' => Array($root['news'],$root['tags'],$root['link'])),
		   Array('title'=>'用户管理','channels' => Array())
		);
		//添加拓展应用菜单,拓展应用的所有功能未定义到数据库，而是在appmanageApi.php中自己定义
		//print_r($menu);
		foreach ($pluginlist as $vo){
			$menu[3]['channels'][]=$root[$vo];
		}
		//添加答疑管理
		$menu[3]['channels'][]=$root['msg'];
		
		$menulist= json_encode($menu);
		//print_r($menulist);
		$this->menulist=$menulist;
		$this->username=$_SESSION['admin_realname'];
		//新版本更新,后台点击栏目对于栏目首页内容块出现
		$this->framurl=$_GET['url']?urldecode($_GET['url']):url('index/welcome');//内部iframe显示页
		$menuindex=intval($_GET['menuindex']);
		$this->menuindex=$menuindex?$menuindex:0;//设置初始显示菜单
		$this->display();
	}
	
	//登录页面
	public function login()
	{
		if(!$this->isPost())
		{
			//没有提交，就显示登陆的页面
			$this->ver=config('ver_name');//设置模板变量ver 值为config.php里面的ver_name
			$this->display();
		}else{
		//提交了，说明点击登陆了，进行登陆验证处理
		$username=in($_POST['username']);//in是cp里面的过滤函数
		$password=$this->newpwd($_POST['password']);//密码加密
		//数据验证
		if(empty($username))
		{
			$this->error('请输入用户名');
		}
		if(empty($_POST['password']))
		{
			$this->error('请输入密码');
		}
		if(empty($_POST['checkcode']))
		{
			$this->error('请输入验证码');
		}
		if($_POST['checkcode']!=$_SESSION['verify'])
		{
			$this->error('验证码错误，请重新输入');
		}
		/**
		 * 验证码通过之后清楚验证码session，避免机器测试
		 * @author 杨真
		 */
		unset($_SESSION['verify']);
		//如果登陆，session设置
		if(model('admin')->login($username,$password))
		{
			//成功登陆的跳转,跳转到首页
			$this->redirect(url('index/index'));
		}
		else
		{
			$this->error('用户名、密码错误或账户已经被锁定~');
		}
	  }
	}
	
	//用户退出
	public function logout()
	{
		Auth::clear();//清除的不是session ,而是auth权限
		$this->success('您已成功退出系统',url('index/login'));
	}
	
	//生成验证码，调用cp框架里面的函数
	public function verify()
	{
		Image::buildImageVerify();
	}
	
	//自动跳转加载welcome函数
	public function welcome()
	{
		$this->ver=config('ver_name');
		$this->display();
	}
}
