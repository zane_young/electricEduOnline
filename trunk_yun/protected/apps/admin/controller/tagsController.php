<?php
/*
 * 标签管理控制器
 * */
class tagsController extends commonController
{
	public function index()
	{
		$listRows=10;//每页显示的信息条数
		$url=url('tags/index',array('page'=>'{page}'));
		//echo $url;
	    $limit=$this->pageLimit($url,$listRows);

		$count=model('tags')->count();
		$list=model('tags')->select('','','id DESC',$limit);
		$this->page=$this->pageShow($count);//显示分页
		$this->list=$list;
		$this->url=url('link');
		$this->display();
	}
	
	//生成标签
    public function add()
	{
      if(!$this->isPost()){
      	//未提交显示，显示添加的栏目是新闻栏目
      		$where="type=1";
      		$list=model('sort')->select($where,'id,name,type,deep,path,norder');
			if(!empty($list)){
				$list=re_sort($list);
				$this->list=$list;
			}
			$this->display();
		}else{
			//ajax处理
			$sortid=intval($_POST['sortid']);
			$num=intval($_POST['num']);
			if(empty($sortid)){
			   $alist=model('news')->select("ispass='1'",'keywords','hits desc','0,500');
			   $plist=model('photo')->select("ispass='1'",'keywords','hits desc','0,500');
			   $list=@array_merge($alist,$plist);
			   unset($alist);
			   unset($plist);
			}else{
				$typea=model('sort')->find("id='{$sortid}'",'type');
			    switch ($typea['type']) {
			    	case 1:
			    		$list=model('news')->select("ispass='1' AND sort like'%{$sortid}%'",'keywords','hits desc','0,1000');
			    		break;
			    	case 2:
			    		$list=model('photo')->select("ispass='1' AND sort like'%{$sortid}%'",'keywords','hits desc','0,1000');
			    		break;
			    	default:
			    		# code...
			    		break;
			    }
			}
			$allnum=0;
			if(!empty($list)){
				foreach ($list as $vo) {
					$enum=$this->crtags($vo['keywords']);
					if(!empty($num) && $allnum>$num) break;
					if(!empty($enum)) $allnum+=$enum;
				}
			}
			echo $allnum;
		}
	}
	
	//ajax删除，也可以使用传参数删除
	public function del()
	{
		if(!$this->isPost()){
			//未提交是删除单个文件
			$id=intval($_GET['id']);//ajax获得的get id
			if(empty($id)) $this->error('您没有选择~');
			if(model('tags')->delete("id='$id'"))
			echo 1;
			else echo '删除失败~';
		}else{
			//提交时批量删除,type=submit删除
			if(empty($_POST['delid'])) $this->error('您没有选择~');
			$delid=implode(',',$_POST['delid']);//进行批量删除
			if(model('tags')->delete('id in ('.$delid.')'))
			$this->success('删除成功',url('tags/index'));
		}
	}
    
	//编辑点击
	public function hits()
	{
		$id=intval($_POST['id']);
		$hit['hits']=intval($_POST['hits']);
		if(model('tags')->update("id='{$id}'",$hit))
		echo 1;
		else echo '操作失败~';
	}
	//更新文档数量
	public function mesup()
	{
		$id=intval($_POST['id']);
		$namea=model('tags')->find("id='{$id}'",'name');
		//print_r($namea);
		if(empty($namea)) $num=0;
		else{
		 //查询相关的文档
          $name=$namea['name'];
          $where="title like '%".$name."%' OR description like '%".$name."%'";
          $count1=model('news')->count($where);
          $count2=model('photo')->count($where);
          $num=$count1+$count2;
		}
		model('tags')->update("id='{$id}'","mesnum={$num}");
		echo $num;
	}
}