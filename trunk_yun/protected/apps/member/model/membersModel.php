<?php
class membersModel extends baseModel{
	protected $table = 'members';
	
	//按条件检索
	public function memberANDgroup($keyword='',$limit=''){
		//组合sql查询
		$where=(empty($keyword)?'':'where '.$this->prefix.'members.account like "%'.$keyword.'%"');
		//现在连表查询只能使用左连接了，不可用where连表了
		$sql="SELECT {$this->prefix}members.id,{$this->prefix}members.groupid,{$this->prefix}members.account,{$this->prefix}members.nickname,{$this->prefix}members.regip,{$this->prefix}members.lastip,{$this->prefix}members.regtime,{$this->prefix}members.lasttime,{$this->prefix}members.islock,{$this->prefix}member_group.name FROM {$this->prefix}members left outer join {$this->prefix}member_group on {$this->prefix}members.groupid={$this->prefix}member_group.id  {$where} ORDER BY {$this->prefix}members.groupid,{$this->prefix}members.id LIMIT {$limit}";
		return $this->model->query($sql);
	}
	
	//符合条件的会员数
	public function memberscount($keyword="")
	{
		$where=(empty($keyword)?'':'account like "%'.$keyword.'%"');
		return $this->count($where);
	}
	
}
?>